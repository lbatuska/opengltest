// opengltest.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include "Display.h"
#include <GL/glew.h>
#include "Shader.h"
#include "Mesh.h"
#include "Texture.h"
#include "Transform.h"
#include "Camera.h"


#define WIDTH 800
#define HEIGHT 600

int main(int argc, char** argv)
{
	std::cout << "Running -> ";
	std::cout << argv[0];
	std::cout << "\n";

	Display display(WIDTH, HEIGHT, "TestWindow");


	Shader shader("./res/basicShader");

	Texture texture("./res/bricks.jpg");

	Transform transform;

	Camera camera(glm::vec3(0, 0, -4), 70.0f, (float)WIDTH / (float)HEIGHT, 0.01f, 1000.0f);

	float counter = 0.0f;

	Vertex vertices[] = { Vertex(glm::vec3(0.5,-0.5,0),glm::vec2(0.0,0.0)),
						Vertex(glm::vec3(1,0.5,0),glm::vec2(0.5,1.0)),
						Vertex(glm::vec3(1,0.5,0),glm::vec2(0.5,1.0)),
						Vertex(glm::vec3(1.5,-0.5,0),glm::vec2(1.0,0.0))
	};


	unsigned int indices[] = {0,2,3,
								0,3,2};

	Mesh mesh(vertices, sizeof(vertices) / sizeof(vertices[0]), indices, sizeof(indices) / sizeof(indices[0]));

	Mesh mesh2("./res/monkey3.obj");


	while (!display.IsClosed())
	{
		display.Clear(0.0f, 0.15f, 0.3f, 1.0f);

		float sinCounter = sinf(counter);
		float absSinCounter = abs(sinCounter);

		//transform.GetPos()->x = sinCounter;
		transform.GetRot().y = counter * 100;
		transform.GetRot().z = counter * 100;
		//transform.GetScale()->x = absSinCounter;
		//transform.GetScale()->y = absSinCounter;

		shader.Bind();
		texture.Bind(0);
		shader.Update(transform, camera);
		//mesh.Draw();
		mesh2.Draw();

		display.Update();
		counter += 0.00001f;
	}


	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
