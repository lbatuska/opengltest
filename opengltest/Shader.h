#pragma once

#include <string>
#include <GL/glew.h>
#include "Transform.h"
#include "Camera.h"

class Shader
{
public:
	Shader(const std::string& fileName);

	void Bind(); // set gpu to use the vertex & fragment shader that is part of this class

	void Update(const Transform& transform, const Camera& camera);
	

	virtual ~Shader();

private:
	static const unsigned int NUM_SHADERS = 2; // 2 because fragment + vertex shader (no geometry shader now)

	// text = full text of the shader
	GLuint CreateShader(const std::string& text, GLenum shaderType);
	void CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string& errorMessage);
	std::string LoadShader(const std::string& fileName);

	Shader(const Shader& other) {}
	void operator=(const Shader& other) {} // should return Shader&


	enum
	{
		TRANSFORM_U,

		NUM_UNIFROMS
	};


	GLuint m_program; // where the program is
	GLuint m_shaders[NUM_SHADERS];
	GLuint m_uniforms[NUM_UNIFROMS];
};

