#pragma once

#include <string>

#include <SDL2/SDL.h>

class Display
{
public:
	Display(int width, int height, const std::string& title);

	void Update();
	bool inline IsClosed() { return m_isClosed; }
	void Clear(float r, float g, float b, float a);

	virtual ~Display();
private:
	Display(const Display& other) = delete;
	void operator=(const Display& other) = delete;

	SDL_Window* m_window;
	SDL_GLContext m_glContext;
	bool m_isClosed;
};

